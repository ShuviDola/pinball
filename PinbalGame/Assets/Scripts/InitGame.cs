﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitGame : MonoBehaviour
{   
    public GameObject ball;
    public GameObject cameraGame;
    public GameObject cameraInit;

    void OnMouseDown()
    {
        GetComponent<AudioSource>().Play();
        cameraInit.SetActive(false);
        cameraGame.SetActive(true);
        ball.SetActive(true);
    }
}
