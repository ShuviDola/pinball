﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bumper : MonoBehaviour
{
    [SerializeField]
    private float force = 100f;
    [SerializeField]
    private float forceRadius = 1.0f;
    [SerializeField]
    private Light lightBumper;
    private bool lightActivated = false;
    private float tLight = 0;

    public Text scoreText;
    int score;

    void OnCollisionEnter(Collision other)
    {
        foreach (Collider col in Physics.OverlapSphere(transform.position, forceRadius))
        {
            if (col.GetComponent<Rigidbody>())
            {
                col.GetComponent<Rigidbody>().AddExplosionForce(force, transform.position, forceRadius);
                lightActivated = true;
                tLight = 0;
                score = int.Parse(scoreText.text);
                score += 100;
                scoreText.text = score+"";
                GetComponent<AudioSource>().Play();
            }
        }
    }
    void Update()
    {
        if (lightActivated)
        {
            lightBumper.enabled = true;
            tLight = tLight + Time.deltaTime;
            if (tLight >= 0.5f)
            {
                lightBumper.enabled = false;
                tLight = 0;
                lightActivated = false;
            }
        }
    }
}
