using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plunger : MonoBehaviour {

   SpringJoint spring;
    Rigidbody rb;
  
    AudioSource shot;
   [SerializeField] float maxPosition = 3F;

    private bool downPressedDown, downPressed, downPressedUp;

   void Start () {
       spring = GetComponent<SpringJoint>();
        rb = GetComponent<Rigidbody>();
        downPressedDown = downPressed = downPressedUp = false;
        shot = GetComponent<AudioSource>();
   }

    void Update() {
        if(Input.GetKeyDown("space")){
           downPressedDown=true;
            downPressed = false;
            downPressedUp = false;
       }
        else if(Input.GetKey("space")){
            downPressedDown=false;
            downPressed = true;
            downPressedUp = false;
        }else if(Input.GetKeyUp("space")){
           downPressedDown=false;
            downPressed = false;
            downPressedUp = true;
            shot.Play();
       }else{
            downPressedDown=false;
            downPressed = false;
            downPressedUp = false;
        }
    }

   void FixedUpdate()
   {
       if(downPressedDown){
           spring.maxDistance=maxPosition;
       }
        
        if(downPressed){
            rb.AddForce(0,0,-300,ForceMode.Acceleration);
        }
        
        if(downPressedUp){
           spring.maxDistance=0;
       }
   }

}
