﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReloadBall : MonoBehaviour
{
    public GameObject targetReset;
    
    public GameObject check;
    
    public GameObject chequet;

    public GameObject camaraInit;
    
    public GameObject camaraGame;
    
    public GameObject ball;
    public Text scoreText;
    int score;
    public Text vidasText;
    int vidas;
    void Start(){
        vidas = 3;
    }


    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "ball"){

            other.gameObject.transform.position = targetReset.transform.position;
            
            vidas = int.Parse(vidasText.text);
            vidas -= 1;
            

            if(vidas<1){
                score = int.Parse(scoreText.text);
                score = 0;
                scoreText.text = score+"";
                vidas = 3;
                camaraInit.SetActive(true);
                camaraGame.SetActive(false);
                ball.SetActive(false);
            }

            vidasText.text = vidas+"";

            check.SetActive(true);
            chequet.SetActive(false);

        }
    }
}
