﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Downs : MonoBehaviour
{

    public Text scoreText;
    int score;
    public GameObject chequet;
    public static int countDowns = 0;
    public GameObject Down1;
    public GameObject Down2;
    public GameObject Down3;
    public GameObject Down4;
    public GameObject Down5;
    public GameObject Down6;
    Vector3 reset = new Vector3(0, 0.85f, 0);

    Vector3 resetDown1;
    Vector3 resetDown2;
    Vector3 resetDown3;
    Vector3 resetDown4;
    Vector3 resetDown5;
    Vector3 resetDown6;

    void Start(){
        resetDown1 = Down1.transform.position;
        resetDown2 = Down2.transform.position;
        resetDown3 = Down3.transform.position;
        resetDown4 = Down4.transform.position;
        resetDown5 = Down5.transform.position;
        resetDown6 = Down6.transform.position;
    }
    void Update(){

        if(Downs.countDowns > 5){
            Down1.transform.position = resetDown1;
            Down2.transform.position = resetDown2;
            Down3.transform.position = resetDown3;
            Down4.transform.position = resetDown4;
            Down5.transform.position = resetDown5;
            Down6.transform.position = resetDown6;

            score = int.Parse(scoreText.text);
            score += 1500;
            scoreText.text = score+"";
            countDowns = 0;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "down"){
            other.gameObject.transform.position = new Vector3(other.gameObject.transform.position.x, -0.85f, other.gameObject.transform.position.z);
            score = int.Parse(scoreText.text);
            score += 150;
            scoreText.text = score+"";
            countDowns++;
        }   
        
    }

    void OnTriggerEnter(Collider other)
    {

        if(other.tag == "check"){
            other.gameObject.SetActive(false);
            chequet.SetActive(true);
        }   

        
    }

}
