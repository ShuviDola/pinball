﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField] float smoothTime = 1.0f;
    private Vector3 velocity = Vector3.zero;
    [SerializeField] Transform ball;

    private Vector3 targetPosition;

    // Update is called once per frame
    void Update()
    {
        targetPosition.x =this.transform.position.x;
        targetPosition.y = this.transform.position.y;
        targetPosition.z = ball.position.z - 5;
        
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
    }
}
